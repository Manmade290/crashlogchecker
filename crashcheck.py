# inclue lib
import os
import re
import sys
import shutil
import psutil
import datetime
import requests
import colorama
import subprocess
import webbrowser
#os.remove('crashcheck.exe')
#=================================================================================

## 全局版本号
Global_version_SOFT = "1.2" #本地
Global_version_GitLab = "0" #待获取

## 获取执行路径
executable_file = sys.argv[0] or os.path.abspath(__file__)
#print(executable_file) 

# 删除原有的肯德基文件
if os.path.exists('crash_temp'):
    os.remove('crash_temp')

## 获取当前进程名
def get_current_process_name():
    process = psutil.Process()
    return process.name()
if __name__ == "__main__":
    current_process_name = get_current_process_name()
    #print("Current process name:", current_process_name) #crashcheck.exe

#=================================================================================

# check ANSI support function
def supports_ansi():
    if os.name == "nt":
        # Windows
        try:
            colorama.init()
            return True
        except:
            return False
    else:
        # not Windows
        term_look = os.environ.get("TERM")
        return term_look and "256color" in term_look.lower()

# check ANSI support or not
if supports_ansi():
    ANSI_support = True
    #print("ANSI 支持")
else:
    ANSI_support = False
    #print("ANSI 不支持")


#=================================================================================

network_error_occur = None
## 获取远端版本号
# version define
local_version_SOFT = Global_version_SOFT

# url define
version_url = "https://gitlab.com/LeftOwlRight/crashlogchecker/-/raw/main/ver.json?ref_type=heads"
if ANSI_support:
    print("[\033[32m更新\033[0m] 检查更新中...（该流程最多花费\033[34;1m1\033[0m分钟）")
else:
    print("[更新] 检查更新中...（该流程最多花费1分钟）")

try:
    # send HTTP request for JSON data
    response = requests.get(version_url, timeout = 10)
    response.raise_for_status()  # check if succeed

    # decode JSON data
    json_data = response.json()
    remote_version_GitLab = json_data[0]["version"]
    Global_version_GitLab = remote_version_GitLab

    # print("Version:", remote_version_GitLab) # print version
except requests.exceptions.Timeout:
    if ANSI_support:
        print("\033[33m网络连接超时\033[0m")
    else:
        print("网络连接超时")
    network_error_occur = True
except requests.exceptions.RequestException as e:
    if ANSI_support:
        print("[\033[31m错误\033[0m] 发生错误")
        print("\033[33m失败\033[0m：", str(e))
    else:
        print("[错误] 发生错误")
        print("更新检查失败：", str(e))
    network_error_occur = True


#=================================================================================


## 检测目录文件
# 路径和名字
current_exe_dir = os.path.dirname(sys.executable)
detect_filename = "PD2-CrashChecker-2.0.exe"
# 拼接文件的完整路径
detect_file_path = os.path.join(current_exe_dir, detect_filename)
# 检测文件是否存在
# 这里会有一个问题，如果本地文件落后于远程文件，但二者文件名相同，会导致程序认为本地版本已更新，而继续使用本地落后文件
if os.path.isfile(detect_file_path):
    Update_File_Exist = True
else:
    Update_File_Exist = False


#=================================================================================

update_file = None
Update_Downloaded = None
## 下载更新
if not network_error_occur and local_version_SOFT != Global_version_GitLab and not Update_File_Exist:
    if ANSI_support:
        print("\033[32m发现新版本，开始更新...\033[0m")
    else:
        print("发现新版本，开始更新...")
    # 下载更新文件
    update_download_url = "https://gitlab.com/LeftOwlRight/crashlogchecker/-/raw/main/crashcheck.exe?ref_type=heads&inline=false"
    try:
        response = requests.get(update_download_url, timeout = 50)
        response.raise_for_status()

        # 这里改名字是为了防止远程文件与本地文件重名带来的冲突，同时也方便更新之后删除旧文件
        os.rename(os.path.basename(sys.argv[0]), 'crash_temp')

        # 将更新文件保存到本地
        update_file = "PD2-CrashChecker-2.0.exe"
        with open(update_file, "wb") as f:
            f.write(response.content)

        Update_Downloaded = True
    except requests.exceptions.Timeout:
        if ANSI_support:
            print("\033[33m网络连接超时\033[0m")
        else:
            print("网络连接超时")
    except requests.exceptions.RequestException as ec:
        if ANSI_support:
            print("[\033[31m错误\033[0m] 无法下载更新文件")
            print("\033[33m失败\033[0m：", str(ec))
        else:
            print("[错误] 无法下载更新文件")
            print("失败：", str(ec))
        network_error_occur = True

    network_error_occur = True
else:
    # no need to update
    if not network_error_occur:
        if ANSI_support:
            print("\033[32m已是最新版本！\033[0m")
        else:
            print("已是最新版本！")
        

Run_New = None
if Update_Downloaded:
    if ANSI_support:
        print("\033[32m成功\033[0m：更新完毕！运行新版程序...")
    else:
        print("成功：更新完毕！运行新版程序...")
    # 运行更新程序
    try:
        # 这里必须使用Popen才能保证删除旧文件，使用call会导致父进程杀不死从而无法删除旧文件
        subprocess.Popen([update_file])
        Run_New = True
    except:
        if ANSI_support:
            print("[\033[31m错误\033[0m] 执行新版程序出错")
        else:
            print("[错误] 执行新版程序出错")
        print("将运行旧版程序")
    if Run_New:
        sys.exit(0)


if network_error_occur:
    if ANSI_support:
        print("[\033[33m警告\033[0m] 更新失败")
    else:
        print("[警告] 更新失败")
        
        
print("="*100)

#=================================================================================


## 访问闪退日志
# access crash.txt
AppPath = os.getenv('localappdata')
AppData = ''

try:
    with open(AppPath+'\PAYDAY 2\crash.txt', 'r', encoding='utf-8') as file:
        AppData = file.read().replace('/', '\\')
        AppData_Frontline = ''.join(file.readlines()[:4]).replace('/', '\\')
except:
    try:
        with open(AppPath+'\PAYDAY 2\crash.txt', 'r', encoding='gbk') as file:
            AppData = file.read().replace('/', '\\')
            AppData_Frontline = ''.join(file.readlines()[:4]).replace('/', '\\')
    except:
        if ANSI_support:
            print("[{}错误{}] 日志读取失败".format('\033[31m', '\033[0m'))
        else:
            print("[错误] 日志读取失败")
        error_occur = True


#=================================================================================

error_occur = None
## 获取文件修改时间
# get file modified time
file_path_datetime = os.path.join(AppPath, 'PAYDAY 2', 'crash.txt')

if os.path.exists(file_path_datetime):
    modification_time = os.path.getmtime(file_path_datetime)
    modification_datetime = datetime.datetime.fromtimestamp(modification_time)
    print("闪退报告的生成时间：", modification_datetime)
    print("请检查生成时间是否符合闪退发生的时间\n   ——若不符合，则你的问题没有闪退报告生成，请向相关社区求助")
else:
    if ANSI_support:
        print("{}失败{}：文件不存在\n\n".format('\033[33m', '\033[0m'))
    else:
        print("失败：文件不存在\n\n")
    error_occur = True

if error_occur:
    input("按下回车键退出...")
    sys.exit(1)


#=================================================================================



def Pattern_All(text):
    Pattern = re.compile(text)
    return Pattern.findall(AppData)

def Script_Small_or_Big():
    if Pattern_All(r'Script stack'):
        return 'small'
    elif Pattern_All(r'SCRIPT STACK'):
        return 'big'
    else:
        return None
    
Script_Letter = Script_Small_or_Big()


def Current_Pattern(regular_letter):
    pattern = re.compile(regular_letter)
    content_exist = pattern.search(AppData)
    if content_exist:
        content = content_exist.group()
        #print(content)
        return content

def Pattern_Application(text):
    content = Current_Pattern(r'(?<=Application has crashed:)[\s\S]*?(?=--------------------)')
    if content:
        if text in content:
            return text
        
def Pattern_Callstack(text):
    content = Current_Pattern(r'(?<=Callstack:)[\s\S]*?(?=--------------------)')
    if content:
        if text in content:
            return text
        
def Pattern_Currentthread(text):
    content = Current_Pattern(r'(?<=Current thread:)[\s\S]*?(?=--------------------)')
    if content:
        if text in content:
            return text

def Pattern_Scriptstack(text):
    pattern_big = re.compile(r'(?<=SCRIPT STACK)[\s\S]*?(?=--------------------)') # re.compile(r'(?<=SCRIPT STACK)[\s\S]*(?=-------------------------------\s*Callstack:)')
    pattern_small = re.compile(r'(?<=Script stack)[\s\S]*?(?=--------------------)') # re.compile(r'(?<=Script stack)[\s\S]*(?=-------------------------------\s*Callstack:)')
    content_exist = pattern_big.search(AppData)
    if content_exist:
        content = content_exist.group()
        if text in content:
            return text
    else:
        content_exist = pattern_small.search(AppData)
        if content_exist:
            content = content_exist.group()
            if text in content:
                return text
            
#================================================================================

CPU_SSE_support1 = Pattern_All(r'(SSE4.1)')
CPU_SSE_support2 = Pattern_All(r'(SSE4.2)')

if not CPU_SSE_support1 and not CPU_SSE_support2:
    print("[您的CPU不支持SSE4.1和SSE4.2指令集，无法使用Modern CPU Support (2021+)启动游戏]")
elif not CPU_SSE_support1:
    print("[您的CPU不支持SSE4.1指令集，可能无法使用Modern CPU Support (2021+)启动游戏]")
elif not CPU_SSE_support2:
    print("[您的CPU不支持SSE4.2指令集，可能无法使用Modern CPU Support (2021+)启动游戏]")
else:
    print("[您的CPU支持SSE4.1和SSE4.2指令集]")

print("-"*100)
print("日志自动分析如下：")

##test
#Pattern_Application('d')
#Pattern_Callstack('d')
#Pattern_Currentthread('d')

Patternmanager = re.compile(r'(blackmarketmanager)')
Blackmarket = Patternmanager.findall(AppData)

if Blackmarket:
    print('检测到你的装备库存档损坏：请用如下mod修复：')
    print("自动打开链接：https://pan.baidu.com/s/15Hxa5iw6mNdOoWT1HgtJZg?pwd=eeee 提取码：eeee\n")
    webbrowser.open('https://pan.baidu.com/s/15Hxa5iw6mNdOoWT1HgtJZg?pwd=eeee')


PatternModName = re.compile(r'mods\\(.*?)\\')
PatternLuaName = re.compile(r'\\.*\\(.*?):')
PatternErr = re.compile(r': attempt to index global \'(.*)\'')

ModName = PatternModName.findall(AppData)
LuaName = PatternLuaName.findall(AppData)
Err = PatternErr.findall(AppData)
if ModName:
    PatternMod_p1 = re.compile(r'(No Mutants Allowed)')
    Mod_p1 = PatternMod_p1.findall(AppData)

    PatternMod_p2 = re.compile(r'(Hotline-Miami)')
    Mod_p2 = PatternMod_p2.findall(AppData)


PatternAcc = re.compile(r'(access violation)')
Acc_Exist = PatternAcc.findall(AppData)

PatternScriptStack = re.compile(r'(Script stack)')
ScriptStack_Exist = PatternScriptStack.findall(AppData)

PatternUpdate = re.compile(r'(update)')
Update = PatternUpdate.findall(AppData)

Pattern_upd_actions = re.compile(r'(_upd_actions)')
upd_actions = Pattern_upd_actions.findall(AppData)

PatternCopactionhurt = re.compile(r'(copactionhurt)')
Copactionhurt = PatternCopactionhurt.findall(AppData)

PatternCRF = re.compile(r'(Cannot read from file)')
CRF = PatternCRF.findall(AppData)


PatternCop = re.compile(r'(cop)')
Cop = PatternCop.findall(AppData)

PatternTex = re.compile(r'(not load texture)')
Tex = PatternTex.findall(AppData)






PatternLib = re.compile(r'\[string (.*)\]')
Lib = PatternLib.findall(AppData)
if Lib:
    PatternLib_p1 = re.compile(r'(explosionmanager)')
    Lib_p1 = PatternLib_p1.findall(Lib[0])

    PatternLib_p2 = re.compile(r'(blackmarketgui)')
    Lib_p2 = PatternLib_p2.findall(Lib[0])

    PatternLib_p3 = re.compile(r'(blackmarketmanager)')
    Lib_p3 = PatternLib_p3.findall(Lib[0])
    if Lib_p3:
        PatternLib_p3_1 = re.compile(r'(table)')
        Lib_p3_1 = PatternLib_p3_1.findall(AppData)

        PatternLib_p3_2 = re.compile(r'(skin)')
        Lib_p3_2 = PatternLib_p3_2.findall(AppData)

    PatternCore = re.compile(r'coreunitdamage')
    Core = PatternCore.findall(Lib[0])

    PatternLib_p4 = re.compile(r'(criminalsmanager)')
    Lib_p4 = PatternLib_p4.findall(Lib[0])

    PatternLib_p5 = re.compile(r'(doctorbagbase)')
    Lib_p5 = PatternLib_p5.findall(Lib[0])

    PatternLib_p6 = re.compile(r'(contourext)')
    Lib_p6 = PatternLib_p6.findall(Lib[0])

    PatternLib_p7 = re.compile(r'(mousepointermanager)')
    Lib_p7 = PatternLib_p7.findall(Lib[0])

    PatternLib_p8 = re.compile(r'(newraycastweaponbase)')
    Lib_p8 = PatternLib_p8.findall(Lib[0])

    PatternLib_p9 = re.compile(r'(playermanager)')
    Lib_p9 = PatternLib_p9.findall(Lib[0])

    PatternLib_p10 = re.compile(r'(basenetworkhandler)')
    Lib_p10 = PatternLib_p10.findall(Lib[0])

    PatternLib_p11 = re.compile(r'(sentrygunweapon)')
    Lib_p11 = PatternLib_p11.findall(Lib[0])

    PatternLib_p12 = re.compile(r'(unitnetworkhandler)')
    Lib_p12 = PatternLib_p12.findall(Lib[0])


match = False


if ModName and not CRF:
    print('您的模组[' + ModName[0] + ']在代码文件的' + LuaName[0] + '上出现了问题，请检查这个模组')
    if Mod_p1:
        print('检测到可能是NoMA出现问题\n在确保你使用最新版本的NoMA并使用了更新工具的前提下，'
              '该闪退可能是由于你使用了某个mod'
              '大幅度更改或重做了敌人的伤害及其相关机制导致NoMA无法正常检测。')

    if Mod_p2:
        print('检测到可能是因为火线迈阿密(Hotline-Miami)HUD改名导致的问题')
    if Err:
        print('模组在[' + Err[0] + ']上存在空值')
    print('Vinight提示：若删除模组依然出现闪退，那么请验证游戏文件完整性')
    match = True

if Acc_Exist and Update and upd_actions and Copactionhurt:
    print('这是一个确定存在于游戏版本1.143.243的问题')
    print('安装下面这个模组修复：'
          '\n自动打开链接：https://modworkshop.net/mod/46649')
    webbrowser.open('https://modworkshop.net/mod/46649')
    match = True

if Acc_Exist and not ScriptStack_Exist:
    print('这是一个无法准确定位问题所在的闪退')
    print('这种情况可能是游戏或者是模组的问题，可能的原因及解决方法按可能性从大到小排序如下：'
          '\n     1.内存溢出，32位程序最多使用4GB内存（带有音频、图片、或模型文件的模组会提高内存使用）'
          '\n     ——尝试在视频设置中将纹理质量调低一档。'
          '\n     2.检查你新安装或者是新更新的模组'
          '\n     3.请直接备份模组文件，然后检查游戏文件完整性'
          '\n     4.是否安装违禁模组')
    match = True

if CRF:
    print('因不能读取文件而导致闪退，一般发生在进入游戏的时候，可能是缺少了什么依赖：'
          '\n     1.更新一下你的插件(blt或者是Beardlib)'
          '\n     2.备份一遍模组然后检查游戏完整性')
    print('若有什么更好的解决方法，请联系Vinight，QQ1121169088')

    if Cop:
        print('您的报错似乎提示警察在两个动作之间有异常移动，这属于游戏引擎问题。'
              '\n安装FSS即可减少此类报错')
    match = True


if Lib:
    print('您的游戏的模组或者是游戏本身在使用'+Lib[0]+'这个接口时出现问题，请检查是否有模组使用这个函数')
    print('检查你安装的模组的mod.txt里是否引用此接口\n')
    if Lib_p1:
        print('检测到您这次的报错基本上属于游戏内出现问题，重新上线游玩即可')

    if Lib_p2:
        print('检测到您这次的报错可能属于存档问题，请恢复备份存档')

    if Lib_p3:
        if Lib_p3_1:
            print('检测到您这次的报错可能属于更多预设的模组导致的')
        elif Lib_p3_2:
            print('检测到您这次的报错可能由第三方武器或皮肤引起\n请检查自己的皮肤、面具、放置物模组')
        else:
            print('检测到您这次的报错可能属于错误删除第三方武器导致的存档损坏，使用模组修复')
            print('模组链接：\n'
                  '自动打开链接：https://pan.baidu.com/s/19fy3QNONBSdS4OuJRLGzSA 提取码：1138')
            webbrowser.open('https://pan.baidu.com/s/19fy3QNONBSdS4OuJRLGzSA')

    if Lib_p4:
        print('检测到您这次的报错可能属于某个受到攻击的单位无法受到爆炸伤害\n可能是如下模组导致：\n1.无友军伤害')

    if Lib_p5:
        print('检测到您这次的报错可能属于网络太差，丢包导致的数据异常闪退')

    if Lib_p6:
        print('检测到您这次的报错可能属于FSS和IO(输入输出流)引起的冲突')

    if Lib_p7:
        print('检测到您这次的报错可能属于淘宝买了全dlc作弊后删档所致\n温馨提示：浪子回头金不换')

    if Lib_p8:
        print('检测到您这次的报错可能属于第三方武器残留，自行寻找并删除残留数据的武器')

    if Lib_p9:
        print('检测到您这次的报错可能属于:'
              '\n     1.主副手通用的模组造成的存档数据异常(损坏),报错为:963'
              '\n     2.主武器异常，这是由于你曾经使用第三方武器或武器配件后，在不使用或删除模组时未能完全清除第三方武器数据造成的,报错为:965'
              '\n     3.副武器异常，这是由于你曾经使用第三方武器或武器配件后，在不使用或删除模组时未能完全清除第三方武器数据造成的,报错为:966'
              '\n     4.强制友谊代码异常，报错为：281 修复链接：https://pan.baidu.com/s/1AiYLLywqqIlNy3rr6SPXvQ 提取码：chin')
        webbrowser.open('https://pan.baidu.com/s/1AiYLLywqqIlNy3rr6SPXvQ')

    if Lib_p10:
        print('检测到您这次的报错可能属于HUD或者插件版本太老，请更新HUD(更有可能是pocohud和大胡子(Beardlib)')

    if Lib_p11:
        print('检测到您这次的报错可能属于网络太差在同步炮台子弹时出错(传输数据包出错)')

    if Lib_p11:
        print('检测到您这次的报错可能属于自己或某个玩家安装了一个自动设置炮台为AP模式的模组，自动设置的信息接收早于放置的信息导致的。\n'
              '也可能是正常玩家网络延迟过高，给房主发送了错乱的信息导致的非恶意性炸房。')

    if Core:
        print('检测到您这次的报错可能属于典型的游戏本身问题，电脑因为某种原因承载不起当前状态的计算导致闪退')
    match = True

if Tex:
    print('可能是材质纹理没有足够的内存加载，试着调低材质纹理')
    match = True


if not match:
    print('未收录此类报错\n若报错为空白，可能是未开显卡加速')

print('-'*100)
print("提示:\n本程序进行的自动检测分析仅供参考，若程序分析未能解决问题，请查看完整报告并截图发在贴吧或群聊提问。\n\n检查完整的错误报告请输入Y")
inputStr = input()
if inputStr == 'y' or inputStr == 'Y':
    print(AppData)
    print('软件版本2.0-Alpha，按回车结束软件')
    print('软件制作：Vinight QQ:1121169088')
    print('软件更新：LR_Daring & Vi.night')
    print('闪退日志及解决方法提供：LR_Daring')
inputStr = input()

# pyinstaller --onefile crashcheck.py
# pyinstaller -i f.ico -F 查看闪退日志.py
